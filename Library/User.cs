﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class User
    {
        public string name;
        public string surname;
        public string nick;
        public string password;
        public string status;
        public int numberOfBorrowedBooks;
        public List<Book> borrowedBoks = new List<Book>();

        public User(string st = "User" , int borrowedBooks = 0)
        {
            status = st;
            numberOfBorrowedBooks = borrowedBooks;
        }
    }
}
