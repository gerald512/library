﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Library
{
    class LibraryManagement
    {
        public LibraryManagement(ILibraryOperations operations, string path)
        {
            libraryOperations = operations;
            accessPathToHelp = path;
        }

        public void startPage(User user)
        {
            Console.WriteLine("\nWELCOME {0} TO OUR LIBRARY", user.name);
            Console.WriteLine("Now You can use the library functions.\n");
            Console.WriteLine("If You want see a command list enter 'help'\n");
        }

        public void getOrder(User user, IUserDateBase userBase, List<User> users)
        {
            string order;
            
            do
            {
                order = Console.ReadLine();

                if (order == "help")
                    showHelp();

                else if (order == "swbooks")
                    showBooks(libraryOperations.getBooks());

                else if (order == "borrow")
                    borrow(user, users);

                else if (order == "bringout")
                    bringOut(user, users);

                else if (order == "delbook")
                    delBook(user);

                else if (order == "addbook")
                    addBook(user);

                else if (order == "deluser")
                {
                    delUser(user, users, userBase);

                    if (user.numberOfBorrowedBooks == 0)
                        break;
                }

                else if (order == "srhauthor")
                    srhAuthor();

                else if (order == "borbooks")
                    borBooks(user);

                else if (order == "srhtitle")
                    srhTitle();

                else if (order == "logout")
                    showAccountStartPage();
                                   
                else
                    Console.WriteLine("ERROR: Wrong command!!!");
            } while (order != "logout");
        }

        private void borrow(User user, List<User> users)
        {
            try
            {
                Tuple<string, int> titleAndID = getTitleAndID();
                Book book = libraryOperations.findByTitleAndID(titleAndID);

                if (book.status != "Available")
                    Console.WriteLine("ERROR: This book is borrowed!!!");
                else
                    libraryOperations.borrowBook(book, user, users);
            }
            catch (FormatException)
            {
                Console.WriteLine("ERROR: Wrong input date format!!!");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("ERROR: Don't found books!!!");
            }
        }

        private void bringOut(User user, List<User> users)
        {
            try
            {
                Tuple<string, int> titleAndID = getTitleAndID();
                Book book = libraryOperations.findByTitleAndID(titleAndID);

                if (user.nick != book.whoBorrowed && user.numberOfBorrowedBooks > 0)
                    Console.WriteLine("ERROR: You don't borrow this book!!!");
                else if (user.numberOfBorrowedBooks == 0)
                    Console.WriteLine("ERROR: You haven't any book to bring out to library!!!");

                libraryOperations.bringOutBook(book, user, users);
            }
            catch (FormatException)
            {
                Console.WriteLine("ERROR: Wrong input date format!!!");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("ERROR: Don't found books!!!");
            }
        }

        private void delBook(User user)
        {
            try
            {
                if (user.status != "Administrator")
                    Console.WriteLine("ERROR: You haven't permissions!!!");
                else if (user.status == "Administrator")
                {
                    Tuple<string, int> titleAndID = getTitleAndID();
                    Book book = libraryOperations.findByTitleAndID(titleAndID);
                    libraryOperations.delBook(book);

                    if (book.status == "Unavailable")
                        Console.WriteLine("ERROR: Can't delete this book");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("ERROR: Wrong input date format!!!");
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("ERROR: Don't found books!!!");
            }
        }

        private void addBook(User user)
        {
            try
            {
                if (user.status == "Administrator")
                {
                    Book book = getBook();
                    libraryOperations.addBook(book);
                }
                else
                    Console.WriteLine("ERROR: You haven't permissions!!!");
            }
            catch (FormatException)
            {
                Console.WriteLine("ERROR: Wrong input date format!!!");
            }
        }

        private void delUser(User user, List<User> users, IUserDateBase userBase)
        {
            if (user.numberOfBorrowedBooks != 0)
                Console.WriteLine("ERROR: You must bring out all books to library");
            else
            {
                libraryOperations.delUser(userBase, user, users);
                showAccountStartPage();
            }
        }

        private void srhAuthor()
        {
            string author = getAuthor();
            List<Book> foundBooks = libraryOperations.findByAuthor(author);

            if (foundBooks.Count != 0)
                showBooks(foundBooks);
            else
                Console.WriteLine("ERROR: Don't found books!!!");
        }

        private void borBooks(User user)
        {
            string whoBorrowed = user.nick;
            List<Book> foundBooks = libraryOperations.findByWhoBorrowed(whoBorrowed);

            if (foundBooks.Count != 0)
                showBooks(foundBooks);
            else
                Console.WriteLine("ERROR: Don't found books!!!");
        }

        private void srhTitle()
        {
            string title = getTitle();
            List<Book> foundBooks = libraryOperations.findByTitle(title);

            if (foundBooks.Count != 0)
                showBooks(foundBooks);
            else
                Console.WriteLine("ERROR: Don't found books!!!");
        }

        private void showBooks(List<Book> books)
        {
            foreach (Book book in books)
                Console.WriteLine(book.title + '|' + book.author + '|' + book.publisher + '|' + book.year
                                + '|' + book.type + '|' + book.numberOfPages + '|' + book.status + '|' + book.whoBorrowed + '|' + book.id);
        }

        private void showHelp()
        {
            string commands = File.ReadAllText(accessPathToHelp);
            Console.WriteLine(commands + "\n");
        }

        private Tuple<string, int> getTitleAndID()
        {
            Book book = new Book();

            Console.WriteLine("Enter the title:");
            book.title = Console.ReadLine();
            Console.WriteLine("Enter the id:");
            book.id = int.Parse(Console.ReadLine());

            return new Tuple<string, int>(book.title, book.id);
        }

        private Book getBook()
        {
            Book book = new Book();

            Console.WriteLine("Enter the title:");
            book.title = Console.ReadLine();
            Console.WriteLine("Enter the author:");
            book.author = Console.ReadLine();
            Console.WriteLine("Enter the publisher:");
            book.publisher = Console.ReadLine();
            Console.WriteLine("Enter the year:");
            book.year = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the type:");
            book.type = Console.ReadLine();
            Console.WriteLine("Enter the number of pages:");
            book.numberOfPages = int.Parse(Console.ReadLine());

            var maxId = libraryOperations.getMaxId();
            book.id = ++maxId;
            
            return book;
        }

        private void showAccountStartPage()
        {
            AccountManagement accountManagement = new AccountManagement();
            accountManagement.startPage();
        }

        private string getAuthor()
        {
            Book book = new Book();
            Console.WriteLine("Enter the author:");
            book.author = Console.ReadLine();
            return book.author;
        }

        private string getTitle()
        {
            Book book = new Book();
            Console.WriteLine("Enter the title:");
            book.title = Console.ReadLine();
            return book.title;
        }

        private ILibraryOperations libraryOperations;
        private readonly string accessPathToHelp;
    }
}
