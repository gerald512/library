﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class AccountManagement
    {
        public AccountManagement()
        {
        }

        public AccountManagement(IUserOperations operations)
        {
            userOperations = operations;
        }

        public void startPage()
        {
            Console.WriteLine("\t\tLIBRARY");
            Console.WriteLine("If You want to use the library functions, You must create an account.\n");
            Console.WriteLine("If You have account enter 'login'.");
            Console.WriteLine("If You want creat a account enter 'register'.");
            Console.WriteLine("\nIf You want back to start page enter 'start'\n");
        }

        public void getOrder()
        {
            string order;

            do
            {
                order = Console.ReadLine();

                if (order == "login")
                {
                    Tuple<string, string> nickAndPass = getNickAndPass();
                    User user = userOperations.findByNickAndPass(nickAndPass);

                    if (user != null)
                        userOperations.login(user);
                    else
                        Console.WriteLine("ERROR: Wrong nick or password!!!");
                }

                else if (order == "register")
                {
                    User user = getUserDate();
                    User findUser = userOperations.findByNick(user);

                    if (findUser == null)
                        userOperations.register(user);
                    else
                        Console.WriteLine("ERROR: This nick is unavailable!!!");
                }

                else if (order == "start")
                    startPage();

                else if (order == "exit")
                    Console.WriteLine("The application is closed by clicking the button.");

                else
                    Console.WriteLine("ERROR: Wrong command!!!");

            } while (order != "exit");
        }

        private User getUserDate()
        {
            User user = new User();
            do
            {
                Console.WriteLine("Enter name: ");
                user.name = Console.ReadLine();
            } while (user.name == "");

            do
            {
                Console.WriteLine("Enter surname: ");
                user.surname = Console.ReadLine();
            } while (user.surname == "");
            
            do
            {
                Console.WriteLine("Enter nick: ");
                user.nick = Console.ReadLine();
            } while (user.nick == "");
            
            do
            {
                Console.WriteLine("Enter password: ");
                user.password = Console.ReadLine();
            } while (user.password == "");
            
            return user;
        }

        private Tuple<string, string> getNickAndPass()
        {
            User user = new User();
            do
            {
                Console.WriteLine("Enter nick: ");
                user.nick = Console.ReadLine();
            } while (user.nick == "");

            do
            {
                Console.WriteLine("Enter password: ");
                user.password = Console.ReadLine();
            } while (user.password == "");

            return new Tuple<string, string>(user.nick, user.password);
        }

        private IUserOperations userOperations;
    }
}
