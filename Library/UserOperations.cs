﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class UserOperations : IUserOperations
    {
        public UserOperations(IUserDateBase dataBase, string pathToLibrary, string pathToHelp)
        {
            accessPathToHelp = pathToHelp;
            accessPathToLibrary = pathToLibrary;
            userDateBase = dataBase;
            users = userDateBase.loadUsers();
        }

        public void login(User user)
        {
            BooksDateBase booksDateBase = new BooksDateBase(accessPathToLibrary, '|');
            LibraryOperations libraryOperations = new LibraryOperations(booksDateBase, userDateBase);
            LibraryManagement libraryManagement = new LibraryManagement(libraryOperations, accessPathToHelp);

            libraryManagement.startPage(user);
            libraryManagement.getOrder(user, userDateBase, users);
        }

        public void register(User user)
        {
            users.Add(user);
            userDateBase.saveDate(users);
        }

        public User findByNickAndPass(Tuple<string, string> nickAndPass)
        {
            User findUser = users.Find(x => (x.nick == nickAndPass.Item1) && (x.password == nickAndPass.Item2));
            return findUser;
        }

        public User findByNick(User user)
        {
            User findUser = users.Find(x => x.nick == user.nick);
            return findUser;
        }

        private readonly string accessPathToLibrary;
        private readonly string accessPathToHelp;
        private IUserDateBase userDateBase;
        private List<User> users;
    }
}
