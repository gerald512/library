﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    interface IUserOperations
    {
        void login(User user);
        void register(User user);
        User findByNickAndPass(Tuple<string, string> nickAndPass);
        User findByNick(User user);
    }
}
