﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    interface ILibraryOperations
    {
        void borrowBook(Book book, User user, List<User> users);
        void bringOutBook(Book book, User user, List<User> users);
        void delBook(Book book);
        void addBook(Book book);
        void delUser(IUserDateBase userBase, User user, List<User> users);
        List<Book> findByTitle(string title);
        List<Book> findByAuthor(string author);
        List<Book> findByWhoBorrowed(string whoBorrowed);
        Book findByTitleAndID(Tuple<string, int> book);
        List<Book> getBooks();
        bool isBookWithGivenIdInLibrary(int id);
        int getMaxId();
    }
}
