﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    interface IUserDateBase
    {
        List<User> loadUsers();
        void saveDate(List<User> users);
    }
}
