﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Program
    {
        static void Main(string[] args)
        {
            string accessPathToUsers = "D:\\c# programy\\library\\Library\\AllUsers.txt";
            string accessPathToLibrary = "D:\\c# programy\\library\\Library\\AllBooks.txt";
            string accessPathToHelp = "D:\\c# programy\\library\\Library\\Help.txt";

            if (args.Count() == 1)
                accessPathToUsers = args[0];
            if (args.Count() == 2)
                accessPathToLibrary = args[1];
            if (args.Count() == 3)
                accessPathToHelp = args[2];

            UserDateBase userDateBase = new UserDateBase('|', accessPathToUsers);
            UserOperations userOperations = new UserOperations(userDateBase, accessPathToLibrary, accessPathToHelp);
            AccountManagement accountManagement = new AccountManagement(userOperations);

            accountManagement.startPage();
            accountManagement.getOrder();

            Console.ReadKey();
        }
    }
}
