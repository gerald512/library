﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Library
{
    class BooksDateBase : IBooksDateBase
    {
        public BooksDateBase(string path = "", char separator = '|')
        {
            this.separator = separator;
            accessPath = path;
        }

        public List<Book> loadBooks()
        {
            List<Book> books = new List<Book>();
            string line;
            FileStream listOfBooks = new FileStream(accessPath, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(listOfBooks);

            while((line = sr.ReadLine()) != null)
            {
                Book book = new Book();

                string[] substring = line.Split(separator);

                if (line == "")
                    line.DefaultIfEmpty();
                else
                {
                    book.title = substring[0];
                    book.author = substring[1];
                    book.publisher = substring[2];

                    string strYear = substring[3];
                    int year = Convert.ToInt32(strYear);
                    book.year = year;

                    book.type = substring[4];

                    string strNumberOfPages = substring[5];
                    int numberOfPages = Convert.ToInt32(strNumberOfPages);
                    book.numberOfPages = numberOfPages;

                    book.status = substring[6];
                    book.whoBorrowed = substring[7];

                    string strID = substring[8];
                    int id = Convert.ToInt32(strID);
                    book.id = id;

                    books.Add(book);
                }
            }

            listOfBooks.Close();
            sr.Close();
            return books;
        }

        public void saveDate(List<Book> books)
        {
            FileStream listOfBooks = new FileStream(accessPath, FileMode.Truncate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(listOfBooks);

            foreach (Book book in books)
                sw.WriteLine(book.title + separator + book.author + separator + book.publisher + separator
                            + book.year + separator + book.type + separator + book.numberOfPages + separator
                            + book.status + separator + book.whoBorrowed + separator + book.id);

            sw.Close();
            listOfBooks.Close();
        }

        private readonly string accessPath;
        private char separator;
    }
}
