﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class LibraryOperations : ILibraryOperations
    {
        public LibraryOperations(IBooksDateBase datebase, IUserDateBase usrDate)
        {
            booksDateBase = datebase;
            userDateBase = usrDate;
            books = booksDateBase.loadBooks();
        }

        public void borrowBook(Book book, User user, List<User> users)
        {
            if (book.status == "Available")
            {
                book.status = "Unavailable";
                book.whoBorrowed = user.nick;
                user.numberOfBorrowedBooks++;
                booksDateBase.saveDate(books);
                userDateBase.saveDate(users);
            }
        }

        public void bringOutBook(Book book, User user, List<User> users)
        {
            if (book.status == "Unavailable")
            {
                if (user.numberOfBorrowedBooks > 0)
                {
                    if (user.nick == book.whoBorrowed)
                    {
                        book.status = "Available";
                        book.whoBorrowed = "Nobody";
                        user.numberOfBorrowedBooks--;
                        booksDateBase.saveDate(books);
                        userDateBase.saveDate(users);
                    }
                }
            }
        }

        public void delBook(Book book)
        {
            if (book.status == "Available")
            {
                books.Remove(book);
                booksDateBase.saveDate(books);
            }
        }

        public void addBook(Book book)
        {
            if (books.Count == 0)
                book.id = 1000;

            books.Add(book);
            booksDateBase.saveDate(books);
        }

        public void delUser(IUserDateBase userBase, User user, List<User> users)
        {
            users.Remove(user);
            userBase.saveDate(users);
        }

        public List<Book> findByTitle(string title)
        {
            List<Book> foundBooks = books.FindAll(x => x.title == title);
            return foundBooks;
        }

        public List<Book> findByAuthor(string author)
        {
            List<Book> foundBooks = books.FindAll(x => x.author == author);
            return foundBooks;
        }

        public List<Book> findByWhoBorrowed(string whoBorrowed)
        {
            List<Book> foundBooks = books.FindAll(x => x.whoBorrowed == whoBorrowed);
            return foundBooks;
        }

        public Book findByTitleAndID(Tuple<string, int> book)
        {
            Book foundBook = books.Find(x => (x.title == book.Item1)&&(x.id == book.Item2));
            return foundBook;
        }

        public List<Book> getBooks()
        {
            return books;
        }

        public bool isBookWithGivenIdInLibrary(int id)
        {
            return books.Exists(x => x.id == id);
        }

        public int getMaxId()
        {
            var sortBooks = books.OrderBy(book => book.id);
            return sortBooks.Last().id;
        }

        private List<Book> books;
        private IBooksDateBase booksDateBase;
        private IUserDateBase userDateBase;
    }
}
