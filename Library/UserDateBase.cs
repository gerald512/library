﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Library
{
    class UserDateBase : IUserDateBase
    {
        public UserDateBase()
        {

        }

        public UserDateBase(char separator = '|', string path = "")
        {
            this.separator = separator;
            accessPath = path;
        }

        public List<User> loadUsers()
        {
            List<User> users = new List<User>();
            string line;
            FileStream listOfUsers = new FileStream(accessPath, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(listOfUsers);
            
            while ((line = sr.ReadLine()) != null)
            {
                User user = new User();

                string[] substring = line.Split(separator);

                if (line == "")
                    line.DefaultIfEmpty();
                else
                {
                    user.name = substring[0];
                    user.surname = substring[1];
                    user.nick = substring[2];
                    user.password = substring[3];
                    user.status = substring[4];

                    string strNumber = substring[5];
                    int number = Convert.ToInt32(strNumber);
                    user.numberOfBorrowedBooks = number;

                    users.Add(user);
                }
            }

            listOfUsers.Close();
            sr.Close();
            return users;
        }

        public void saveDate(List<User> users)
        {
            FileStream listOfUsers = new FileStream(accessPath, FileMode.Truncate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(listOfUsers);

            foreach (User user in users)
                sw.WriteLine(user.name + separator + user.surname + separator + user.nick + separator
                            + user.password + separator + user.status + separator + user.numberOfBorrowedBooks);

            sw.Close();
            listOfUsers.Close();
        }

        private char separator;
        private readonly string accessPath;
    }
}
