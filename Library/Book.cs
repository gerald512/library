﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Book
    {
        public Book(string st = "Available", string wb = "Nobody")
        {
            status = st;
            whoBorrowed = wb;
        }

        public string title;
        public string author;
        public string publisher;
        public int year;
        public string type;
        public int numberOfPages;
        public string status;
        public string whoBorrowed;
        public int id;
    }
}
